# ~*~ coding: utf-8 ~*~
import time
import os
import datetime
import importlib,sys
importlib.reload(sys)
#import django
#CMDB_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
#CMDB_PATH = os.path.join(BASE_DIR, 'cmdb')
#print (CMDB_PATH)
#sys.path.append(CMDB_PATH)
#os.environ.setdefault("DJANGO_SETTINGS_MODULE","syscmdb.settings")
#django.setup()
from pytz import timezone
from apscheduler.schedulers.background import BackgroundScheduler
#from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from apscheduler.executors.pool import ThreadPoolExecutor, ProcessPoolExecutor
from apscheduler.jobstores.redis import RedisJobStore
#from devops.monitor_items import MonitorItems
#from monitor_items import MonitorItems
#m = MonitorItems()
from tasks import web_monitor

REDIS = {
    'host': '127.0.0.1',
    'port': '6379',
    'db': 3,
    'password': 'jumpserver'
}
#jobstores = {
#    'default': SQLAlchemyJobStore(url='sqlite:tmp/test.db')
#}
jobstores = {
    'redis': RedisJobStore(**REDIS)
}


executors = {
    'default': ThreadPoolExecutor(4),
    'processpool': ProcessPoolExecutor(3)
}
job_defaults = {
    'coalesce': False,
    'max_instances': 3
}
task_scheduler = BackgroundScheduler(jobstores=jobstores, executors=executors, job_defaults=job_defaults,
                                     timezone=timezone('Asia/Shanghai'))

#启动
task_scheduler.start()

#实例化函数
#m = MonitorItems()

# 指定特定的日期触发任务，这类任务只会触发一次
#job = task_scheduler.add_job(my_job, 'date',jobstore='redis', run_date=datetime(2021, 7, 10, 19, 47, 5), args=('date',))
#job = task_scheduler.add_job(my_job, 'interval',jobstore='redis', seconds=3, args=('xiaoming',))
#job = task_scheduler.add_job(web_monitor.delay,'interval',seconds=5,id='job_0',args=('http://10.16.243.152:8081',))


#job.modify(seconds=5)
#删除调度任务
#task_scheduler.remove_job(job)

# 将触发时间间隔修改成 5分钟
#task_scheduler.modify_job('job_0',seconds=5)

#获取当前调度器中的调度列表
task_scheduler.get_jobs()
#删除所有调度列表
task_scheduler.remove_all_jobs()

#关闭调度服务
#task_scheduler.shutdown()

#强制关闭调度服务
#task_scheduler.shutdown(wait=false)

#while True:
#    time.sleep(5)