from django.db import models
from django.forms.fields import IntegerField
class MySQLConfig(models.Model):
	dbname = models.CharField(max_length=80, blank=True, null=True, unique=True, verbose_name="数据库名称")
	username = models.CharField(max_length=80,blank=True, null=True,verbose_name="用户名")
	password = models.CharField(max_length=200,blank=True, null=True,verbose_name="密码")
	ip = models.CharField(max_length=80,blank=True, null=True,verbose_name="ip地址")
	port = models.IntegerField(blank=True, null=True,verbose_name="端口")
	charset = models.CharField(max_length=80,blank=True, null=True,verbose_name="编码")
	status = models.BooleanField(default=True, verbose_name="启用状态")
	frequency_list = [
		(0, '30秒'),
		(1, '1分钟'),
		(2, '5分钟'),
	]
	time_slice = [
		(0, '15分钟', 0.25),
		(1, '30分钟', 0.5),
		(2, '1小时', 1),
		(3, '3小时', 3),
		(4, '6小时', 6),
		(5, '1天', 1),
		(6, '3天', 3),
		(7, '7天', 7),
		(8, '15天', 15),
		(9, '1月', 30),
		(10, '3月', 90),
		(11, '半年', 180),
		(12, '1年', 365),
	]
	mysql_master_slave_list = [
		(0, '无主从'),
		(1, '主'),
		(2, '从'),
	]
	mysql_master_slave = models.IntegerField(default=0,blank=True, null=True,verbose_name="主从类别")
	Slave_IO_Running = models.IntegerField(blank=True, null=True,verbose_name="io线程状态")
	Slave_SQL_Running = models.IntegerField(blank=True, null=True,verbose_name="sql线程状态")
	utime = models.DateTimeField(auto_now=True, null=True, verbose_name="更新时间")


class OracleConfig(models.Model):
	dbname = models.CharField(max_length=80, blank=True, null=True, unique=True, verbose_name="数据库名称")
	username = models.CharField(max_length=80,blank=True, null=True,verbose_name="用户名")
	password = models.CharField(max_length=200,blank=True, null=True,verbose_name="密码")
	ip = models.CharField(max_length=80,blank=True, null=True,verbose_name="ip地址")
	port = models.IntegerField(blank=True, null=True,verbose_name="端口")
	case = models.CharField(max_length=80, blank=True, null=True, verbose_name="实例名称")
	status = models.BooleanField(default=True, verbose_name="启用状态")
	health_status = models.BooleanField(default=True, verbose_name="健康状态")

	frequency_list = [
		(0, '1小时'),
		(1, '1天'),
	]
	time_slice = [
		(0, '15分钟', 0.25),
		(1, '30分钟', 0.5),
		(2, '1小时', 1),
		(3, '3小时', 3),
		(4, '6小时', 6),
		(5, '1天', 1),
		(6, '3天', 3),
		(7, '7天', 7),
		(8, '15天', 15),
		(9, '1月', 30),
		(10, '3月', 90),
		(11, '半年', 180),
		(12, '1年', 365),
	]
	utime = models.DateTimeField(auto_now=True, null=True, verbose_name="更新时间")

class MySQLHistory(models.Model):
	mysqlid = models.IntegerField(blank=True, null=True,verbose_name="mysqlid")
	total_qps = models.IntegerField(blank=True, null=True,verbose_name="总QPS")
	insert_qps = models.IntegerField(blank=True, null=True,verbose_name="插入QPS")
	update_qps = models.IntegerField(blank=True, null=True, verbose_name="修改QPS")
	select_qps = models.IntegerField(blank=True, null=True,verbose_name="查询QPS")
	delete_qps = models.IntegerField(blank=True, null=True, verbose_name="删除QPS")
	commit_tps = models.IntegerField(blank=True, null=True,verbose_name="提交TPS")
	rollback_tps = models.IntegerField(blank=True, null=True, verbose_name="回滚TOPS")
	threads_running = models.IntegerField(blank=True, null=True, verbose_name="当前运行线程数")
	threads_cached = models.IntegerField(blank=True, null=True, verbose_name="缓存可用线程数")
	threads_connected = models.IntegerField(blank=True, null=True, verbose_name="当前连接线程数")
	threads_created = models.IntegerField(blank=True, null=True, verbose_name="新创建的线程数")
	bytes_sent = models.IntegerField(blank=True, null=True, verbose_name="发送流量")
	bytes_received = models.IntegerField(blank=True, null=True, verbose_name="接受流量")
	innodb_buffer_pool_reads = models.IntegerField(blank=True, null=True, verbose_name="从磁盘请求数")
	Innodb_buffer_pool_pages_flushed = models.IntegerField(blank=True, null=True, verbose_name="刷新脏页请求数")
	key_buffer_read_rate = models.FloatField(blank=True, null=True, verbose_name="key读请求命中率")
	key_buffer_write_rate = models.FloatField(blank=True, null=True, verbose_name="key写请求命中率")
	key_blocks_used_rate = models.FloatField(blank=True, null=True, verbose_name="key块使用率")
	Read_Master_Log_Pos = models.IntegerField(blank=True, null=True, verbose_name="读取主文件位置")
	Exec_Master_Log_Pos = models.IntegerField(blank=True, null=True, verbose_name="执行到主文件位置")
	#Slave_IO_Running = models.IntegerField(blank=True, null=True, verbose_name="读取主文件位置")
	#Slave_SQL_Running = models.IntegerField(blank=True, null=True, verbose_name="执行到主文件位置")

	utime = models.DateTimeField(auto_now=True, null=True, verbose_name="更新时间")

	class Meta:
		indexes = [
			models.Index(fields=['utime'])
		]

class OracleHistory(models.Model):
	oracleid = models.IntegerField(blank=True, null=True, verbose_name="oracleid")
	tablespace = models.TextField(blank=True, null=True,verbose_name="表空间")
	utime = models.DateTimeField(auto_now=True, null=True, verbose_name="更新时间")


class UnsignedBigIntegerField(models.BigIntegerField):
	def db_type(self, connection):
		return 'bigint UNSIGNED'

'''	
class SlowQueryReview(models.Model):
	checksum = UnsignedBigIntegerField(max_length=20,blank=True, null=False,primary_key=True,db_index=True, verbose_name="checksum")
	fingerprint = models.TextField(null=False,verbose_name="fingerprint")
	sample = models.TextField(null=False, verbose_name="sample")
	first_seen = models.DateTimeField(null=True, verbose_name="first_seen")
	last_seen = models.DateTimeField(null=True, db_index=True,verbose_name="last_seen")
	reviewed_by = models.CharField(max_length=20, blank=True, null=True, verbose_name="reviewed_by")
	reviewed_on = models.DateTimeField(null=True, verbose_name="reviewed_on")
	comments = models.TextField(null=True, verbose_name="comments")
	class Meta:
		indexes = [models.Index(fields=['checksum'])]


class SlowQueryReviewHistory(models.Model):
	serverid_max = models.SmallIntegerField(max_length=4,null=False,default='0')
	db_max = models.CharField(max_length=100, blank=True, null=True, verbose_name="db_max")
	user_max = models.CharField(max_length=100, blank=True, null=True, verbose_name="user_max")
	checksum = UnsignedBigIntegerField(max_length=20,blank=True, null=False, verbose_name="checksum")
	sample = models.TextField(null=False, verbose_name="sample")
	ts_min = models.DateTimeField(null=False,default='0000-00-00 00:00:00', verbose_name="ts_min")
	ts_max = models.DateTimeField(null=True,default='0000-00-00 00:00:00', verbose_name="ts_max")
	ts_cnt = models.FloatField(blank=True, null=True, verbose_name="ts_cnt")
	Query_time_sum = models.FloatField(blank=True, null=True, verbose_name="Query_time_sum")
	Query_time_min = models.FloatField(blank=True, null=True, verbose_name="Query_time_min")
	Query_time_max = models.FloatField(blank=True, null=True, verbose_name="Query_time_max")
	Query_time_pct_95 = models.FloatField(blank=True, null=True, verbose_name="Query_time_pct_95")
	Query_time_stddev = models.FloatField(blank=True, null=True, verbose_name="Query_time_stddev")
	Query_time_median = models.FloatField(blank=True, null=True, verbose_name="Query_time_median")
	Lock_time_sum = models.FloatField(blank=True, null=True, verbose_name="Lock_time_sum")
	Lock_time_min = models.FloatField(blank=True, null=True, verbose_name="Lock_time_min")
	Lock_time_max = models.FloatField(blank=True, null=True, verbose_name="Lock_time_max")
	Lock_time_pct_95 = models.FloatField(blank=True, null=True, verbose_name="Lock_time_pct_95")
	Lock_time_stddev = models.FloatField(blank=True, null=True, verbose_name="Lock_time_stddev")
	Lock_time_median = models.FloatField(blank=True, null=True, verbose_name="Lock_time_median")
	Rows_sent_sum = models.FloatField(blank=True, null=True, verbose_name="Rows_sent_sum")
	Rows_sent_min = models.FloatField(blank=True, null=True, verbose_name="Rows_sent_min")
	Rows_sent_max = models.FloatField(blank=True, null=True, verbose_name="Rows_sent_max")
	Rows_sent_pct_95 = models.FloatField(blank=True, null=True, verbose_name="Rows_sent_pct_95")
	Rows_sent_stddev = models.FloatField(blank=True, null=True, verbose_name="Rows_sent_stddev")
	Rows_sent_median = models.FloatField(blank=True, null=True, verbose_name="Rows_sent_median")
	Rows_examined_sum = models.FloatField(blank=True, null=True, verbose_name="Rows_examined_sum")
	Rows_examined_min = models.FloatField(blank=True, null=True, verbose_name="Rows_examined_min")
	Rows_examined_max = models.FloatField(blank=True, null=True, verbose_name="Rows_examined_max")
	Rows_examined_pct_95 = models.FloatField(blank=True, null=True, verbose_name="Rows_examined_pct_95")
	Rows_examined_stddev = models.FloatField(blank=True, null=True, verbose_name="Rows_examined_stddev")
	Rows_examined_median = models.FloatField(blank=True, null=True, verbose_name="Rows_examined_median")
	Rows_affected_sum = models.FloatField(blank=True, null=True, verbose_name="Rows_affected_sum")
	Rows_affected_min = models.FloatField(blank=True, null=True, verbose_name="Rows_affected_min")
	Rows_affected_max = models.FloatField(blank=True, null=True, verbose_name="Rows_affected_max")
	Rows_affected_pct_95 = models.FloatField(blank=True, null=True, verbose_name="Rows_affected_pct_95")
	Rows_affected_stddev = models.FloatField(blank=True, null=True, verbose_name="Rows_affected_stddev")
	Rows_affected_median = models.FloatField(blank=True, null=True, verbose_name="Rows_affected_median")
	Rows_read_sum = models.FloatField(blank=True, null=True, verbose_name="Rows_read_sum")
	Rows_read_min = models.FloatField(blank=True, null=True, verbose_name="Rows_read_min")
	Rows_read_max = models.FloatField(blank=True, null=True, verbose_name="Rows_read_max")
	Rows_read_pct_95 = models.FloatField(blank=True, null=True, verbose_name="Rows_read_pct_95")
	Rows_read_stddev = models.FloatField(blank=True, null=True, verbose_name="Rows_read_stddev")
	Rows_read_median = models.FloatField(blank=True, null=True, verbose_name="Rows_read_median")
	Merge_passes_sum = models.FloatField(blank=True, null=True, verbose_name="Merge_passes_sum")
	Merge_passes_min = models.FloatField(blank=True, null=True, verbose_name="Merge_passes_min")
	Merge_passes_max = models.FloatField(blank=True, null=True, verbose_name="Merge_passes_max")
	Merge_passes_pct_95 = models.FloatField(blank=True, null=True, verbose_name="Merge_passes_pct_95")
	Merge_passes_stddev = models.FloatField(blank=True, null=True, verbose_name="Merge_passes_stddev")
	Merge_passes_median = models.FloatField(blank=True, null=True, verbose_name="Merge_passes_median")
	InnoDB_IO_r_ops_min = models.FloatField(blank=True, null=True, verbose_name="InnoDB_IO_r_ops_min")
	InnoDB_IO_r_ops_max = models.FloatField(blank=True, null=True, verbose_name="InnoDB_IO_r_ops_max")
	InnoDB_IO_r_ops_pct_95 = models.FloatField(blank=True, null=True, verbose_name="InnoDB_IO_r_ops_pct_95")
	InnoDB_IO_r_ops_stddev = models.FloatField(blank=True, null=True, verbose_name="InnoDB_IO_r_ops_stddev")
	InnoDB_IO_r_ops_median = models.FloatField(blank=True, null=True, verbose_name="InnoDB_IO_r_ops_median")
	InnoDB_IO_r_bytes_min = models.FloatField(blank=True, null=True, verbose_name="InnoDB_IO_r_bytes_min")
	InnoDB_IO_r_bytes_max = models.FloatField(blank=True, null=True, verbose_name="InnoDB_IO_r_bytes_max")
	InnoDB_IO_r_bytes_pct_95 = models.FloatField(blank=True, null=True, verbose_name="InnoDB_IO_r_bytes_pct_95")
	InnoDB_IO_r_bytes_stddev = models.FloatField(blank=True, null=True, verbose_name="InnoDB_IO_r_bytes_stddev")
	InnoDB_IO_r_bytes_median = models.FloatField(blank=True, null=True, verbose_name="InnoDB_IO_r_bytes_median")
	InnoDB_IO_r_wait_min = models.FloatField(blank=True, null=True, verbose_name="InnoDB_IO_r_wait_min")
	InnoDB_IO_r_wait_max = models.FloatField(blank=True, null=True, verbose_name="InnoDB_IO_r_wait_max")
	InnoDB_IO_r_wait_pct_95 = models.FloatField(blank=True, null=True, verbose_name="InnoDB_IO_r_wait_pct_95")
	InnoDB_IO_r_wait_stddev = models.FloatField(blank=True, null=True, verbose_name="InnoDB_IO_r_wait_stddev")
	InnoDB_IO_r_wait_median = models.FloatField(blank=True, null=True, verbose_name="InnoDB_IO_r_wait_median")
	InnoDB_rec_lock_wait_min = models.FloatField(blank=True, null=True, verbose_name="InnoDB_rec_lock_wait_min")
	InnoDB_rec_lock_wait_max = models.FloatField(blank=True, null=True, verbose_name="InnoDB_rec_lock_wait_max")
	InnoDB_rec_lock_wait_pct_95 = models.FloatField(blank=True, null=True, verbose_name="InnoDB_rec_lock_wait_pct_95")
	InnoDB_rec_lock_wait_stddev = models.FloatField(blank=True, null=True, verbose_name="InnoDB_rec_lock_wait_stddev")
	InnoDB_rec_lock_wait_median = models.FloatField(blank=True, null=True, verbose_name="InnoDB_rec_lock_wait_median")
	InnoDB_queue_wait_min = models.FloatField(blank=True, null=True, verbose_name="InnoDB_queue_wait_min")
	InnoDB_queue_wait_max = models.FloatField(blank=True, null=True, verbose_name="InnoDB_queue_wait_max")
	InnoDB_queue_wait_pct_95 = models.FloatField(blank=True, null=True, verbose_name="InnoDB_queue_wait_pct_95")
	InnoDB_queue_wait_stddev = models.FloatField(blank=True, null=True, verbose_name="InnoDB_queue_wait_stddev")
	InnoDB_queue_wait_median = models.FloatField(blank=True, null=True, verbose_name="InnoDB_queue_wait_median")
	InnoDB_pages_distinct_min = models.FloatField(blank=True, null=True, verbose_name="InnoDB_pages_distinct_min")
	InnoDB_pages_distinct_max = models.FloatField(blank=True, null=True, verbose_name="InnoDB_pages_distinct_max")
	InnoDB_pages_distinct_pct_95 = models.FloatField(blank=True, null=True, verbose_name="InnoDB_pages_distinct_pct_95")
	InnoDB_pages_distinct_stddev = models.FloatField(blank=True, null=True, verbose_name="InnoDB_pages_distinct_stddev")
	InnoDB_pages_distinct_median = models.FloatField(blank=True, null=True, verbose_name="InnoDB_pages_distinct_median")
	QC_Hit_cnt = models.FloatField(blank=True, null=True, verbose_name="QC_Hit_cnt")
	QC_Hit_sum = models.FloatField(blank=True, null=True, verbose_name="QC_Hit_sum")
	Full_scan_cnt = models.FloatField(blank=True, null=True, verbose_name="Full_scan_cnt")
	Full_scan_sum = models.FloatField(blank=True, null=True, verbose_name="Full_scan_sum")
	Full_join_cnt = models.FloatField(blank=True, null=True, verbose_name="Full_join_cnt")
	Full_join_sum = models.FloatField(blank=True, null=True, verbose_name="Full_join_sum")
	Tmp_table_cnt = models.FloatField(blank=True, null=True, verbose_name="Tmp_table_cnt")
	Tmp_table_sum = models.FloatField(blank=True, null=True, verbose_name="Tmp_table_sum")
	Tmp_table_on_disk_cnt = models.FloatField(blank=True, null=True, verbose_name="Tmp_table_on_disk_cnt")
	Tmp_table_on_disk_sum = models.FloatField(blank=True, null=True, verbose_name="Tmp_table_on_disk_sum")
	Filesort_cnt = models.FloatField(blank=True, null=True, verbose_name="Filesort_cnt")
	Filesort_sum = models.FloatField(blank=True, null=True, verbose_name="Filesort_sum")
	Filesort_on_disk_cnt = models.FloatField(blank=True, null=True, verbose_name="Filesort_on_disk_sum")
	Filesort_on_disk_sum = models.FloatField(blank=True, null=True, verbose_name="Merge_passes_median")

	class Meta:
		unique_together = (("checksum", "ts_min","ts_max"),)
		indexes = [models.Index(fields=['serverid_max']),models.Index(fields=['checksum']),models.Index(fields=['Query_time_max'])]

'''




