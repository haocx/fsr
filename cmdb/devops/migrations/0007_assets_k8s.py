# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2020-04-07 11:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devops', '0006_auto_20200407_1057'),
    ]

    operations = [
        migrations.AddField(
            model_name='assets',
            name='k8s',
            field=models.CharField(blank=True, max_length=80, null=True),
        ),
    ]
