from django.shortcuts import render

# Create your views here.

def get_pagerange(page_obj):
    current_index = page_obj.number
    max_pages = page_obj.paginator.num_pages + 1
    start = current_index - 1
    end = current_index + 2
    if end > max_pages:
        start = start - 1
        end = max_pages
    if start < 1:
        if end < max_pages:
            end = end + 1
        start = 1
    return range(start, end)
