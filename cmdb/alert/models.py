from django.db import models
from devops.models import MonitorConfig


class AlertConfig(models.Model):
	host = models.CharField(max_length=30, blank=True, null=True, unique=True, verbose_name="邮件服务器")
	email = models.CharField(max_length=30, blank=True, null=True, unique=True, verbose_name="邮箱")
	password = models.CharField(max_length=30, blank=True, null=True, unique=True, verbose_name="密码")
	port = models.IntegerField(blank=True, null=True, verbose_name="端口")
	cname = models.CharField(max_length=30, blank=True , null=True, unique=True, verbose_name="发件人昵称")
	apitoken = models.CharField(max_length=100, blank=True, null=True, unique=True, verbose_name="apitoken")
	switch_silence = models.CharField(max_length=5,null=True, verbose_name="沉默开关")
	s_time = models.CharField(max_length=10, blank=True, null=True, unique=True, verbose_name="沉默开始时间")
	e_time = models.CharField(max_length=10, blank=True, null=True, unique=True, verbose_name="沉默结束时间")
	valid = models.IntegerField(blank=True, null=True, verbose_name="有效性")

class AlertHistory(models.Model):
	item = models.CharField(max_length=80,blank=True, null=True,verbose_name="告警项目")
	type_list = [
		(0, '站点检测'),
		(1, '端口检测'),
		(2, '进程检测'),
		(3, 'ping检测'),
		(4, '自定义脚本检测')]
	type = models.IntegerField(choices=type_list, blank=True, null=True, verbose_name="类型")
	status = models.BooleanField(verbose_name="状态")
	startime = models.DateTimeField(null=True)
	alertime = models.DateTimeField(auto_now=True, null=True)
	endtime = models.DateTimeField(null=True)

	class Meta:
		ordering = ['-alertime']



class LinkPerson(models.Model):
	name = models.CharField(max_length=10,blank=True, null=True,unique=True,verbose_name="联系人名称")
	mobile = models.CharField(max_length=11,blank=True, null=True,verbose_name="手机号码")
	email = models.CharField(max_length=30,blank=True, null=True,verbose_name="邮箱")
	dingding = models.CharField(max_length=200, blank=True, null=True, verbose_name="钉钉url")
	utime = models.DateTimeField(auto_now=True, null=True, verbose_name="添加时间")

	class Meta:
		ordering = ['-utime']

class LinkGroup(models.Model):
	name = models.CharField(max_length=80,blank=True, null=True,verbose_name="联系人组名称")
	dingurl = models.CharField(max_length=500, blank=True, null=True, verbose_name="钉钉URL")
	info = models.CharField(max_length=200, blank=True, null=True, verbose_name="备注")
	utime = models.DateTimeField(auto_now=True, null=True, verbose_name="添加时间")
	gl = models.ManyToManyField(LinkPerson, related_name='gl', verbose_name='联系组们')
	ml = models.ManyToManyField(MonitorConfig, blank=True,related_name='ml', verbose_name='监控项')


